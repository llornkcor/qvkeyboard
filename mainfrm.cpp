/*
 * This file....
 */

// Copyright (C) 2008 LPotter <lpotter@trolltech.com>
// Copyright (C) 2007 by Todor Gyumyushev <yodor@developer.bg>
// moux http://www.moux.pl
// GPL v2

#include "mainfrm.h"
#include "qmybutton.h"


#include <QDomDocument>
#include <QDomElement>
#include <QDebug>
#include <QDesktopWidget>
#include <QMenu>
#include <QAction>
#include <QPainter>
#include <QDebug>

#include <X11/extensions/XTest.h>
#include <X11/keysym.h>
#include <X11/keysymdef.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>

#include <QX11Info>



mainForm::mainForm(QWidget *parent, Qt::WindowFlags fl):
    QDialog(parent,fl)
{
    //    QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));
    //    QTextCodec::setCodecForCStrings(QTextCodec::codecForName("UTF-8"));
    QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));

    display = QX11Info::display();
    
    QFileInfo info(qApp->arguments()[0]);
    QDir abs(info.absoluteDir());

    shareDir = abs.canonicalPath();

    if(shareDir.contains("bin"))
        shareDir = shareDir.replace("bin", "share");


    setMouseTracking(true);
    mainFormLayout = new QVBoxLayout(this);

    int buttonHeight = 20;
    int buttonwidth = 20;
    int bigButtonWidth = 65;
    int specialButtonWidth = 30;
    
    layKeyb = new QVBoxLayout();
    layKeyb->setSpacing(1);
    layFirstRow = new QHBoxLayout();
    layFirstRow->setSpacing(1);
    int i;
    QMyButton *bt;

    for(i = 0; i < 13; i++) {//Escape F buttonss
        bt  =  new QMyButton(this);
        bt->setMinimumSize(QSize( specialButtonWidth, buttonHeight));
        layFirstRow->addWidget(bt);
        buttons.append(bt);
        bt->setObjectName(QString::number(i));
    }

    layKeyb->addLayout(layFirstRow);

    layRow2 = new QHBoxLayout();
    layRow2->setSpacing(1);

    for(; i < 27; i++) {
        bt =  new QMyButton(this);
        bt->setMinimumSize(QSize( buttonwidth, buttonHeight));
        bt->setObjectName(QString::number(i));
        layRow2->addWidget(bt);
        buttons.append(bt);
    }

    bt = new QMyButton(this); //backspace
    bt->setMinimumSize(QSize(bigButtonWidth, buttonHeight));
    bt->setObjectName(QString::number(i));
    layRow2->addWidget(bt);
    buttons.append(bt);
    i++;
    
    layRow2->addSpacing(12);
    
    bt =  new QMyButton(this); //home
    bt->setMinimumSize(QSize( specialButtonWidth, buttonHeight));
    bt->setObjectName(QString::number(i));
    layRow2->addWidget(bt);
    buttons.append(bt);
    i++;

    layKeyb->addLayout(layRow2);

    layRow3 = new QHBoxLayout();
    layRow3->setSpacing(1);
    
    bt = new QMyButton(this); //tab
    bt->setMinimumSize(QSize(bigButtonWidth-20, buttonHeight));
    bt->setObjectName(QString::number(i));
    layRow3->addWidget(bt);
    buttons.append(bt);
    i++;

    for(; i < 42; i++) {
        bt =  new QMyButton(this);
        bt->setMinimumSize(QSize( buttonwidth, buttonHeight));
        bt->setObjectName(QString::number(i));
        layRow3->addWidget(bt);
        buttons.append(bt);

    }

    bt = new QMyButton(this); //menu
    bt->setMinimumSize(QSize(bigButtonWidth, buttonHeight));
    bt->setObjectName(QString::number(i++));
    layRow3->addWidget(bt);
    buttons.append(bt);
    i++;

    ///////////////////// Menu
    QMenu *optionsMenu;
    optionsMenu = new QMenu("Menu");

    QAction *action;


    action = new QAction("Choose Font", this);
    connect(action, SIGNAL(triggered()), this, SLOT(changeFont()));
    optionsMenu->addAction(action);

    action = new QAction("About", this);
    connect(action, SIGNAL(triggered()), this, SLOT(about()));
    optionsMenu->addAction(action);

    action = new QAction("Exit", this);
    connect(action, SIGNAL(triggered()), qApp, SLOT(quit()));
    optionsMenu->addAction(action);

    bt->setMenu(optionsMenu);

    ///////////////////// Menu

    layRow3->addSpacing(8);

    bt =  new QMyButton(this); //PgUp
    bt->setMinimumSize(QSize( specialButtonWidth, buttonHeight));
    bt->setObjectName(QString::number(i));
    layRow3->addWidget(bt);
    buttons.append(bt);
    i++;

    // here we create list of supported languages

    //  lang->setCheckable(true);
    QDir dir2(shareDir + "/");

    //   QStringList slangs = dir2.entryList(QStringList("*.xml"));
    //     for (uint i = 0; i < slangs.count(); ++i) {
    //       lang->insertItem(slangs[i].remove(".xml"), i);
    //     }
    //     connect(lang, SIGNAL(activated(int)), this, SLOT(changeLanguage(int)));
    //  options->insertItem("&About...", this, SLOT(about()), ALT + Key_S);
    //  pB42->setPopup(options);
    
    layKeyb->addLayout(layRow3);

    layRow4 = new QHBoxLayout();
    layRow4->setSpacing(1);
    
    bt =  new QMyButton(this); //caps
    bt->setMinimumSize(QSize(bigButtonWidth-20, buttonHeight));
    bt->setCheckable(true);
    bt->setChecked(false);
    bt->setObjectName(QString::number(i));
    layRow4->addWidget(bt);
    buttons.append(bt);
    i++;


    for(; i < 56; i++) {
        bt =  new QMyButton(this);
        bt->setMinimumSize(QSize( buttonwidth, buttonHeight));
        bt->setObjectName(QString::number(i));
        layRow4->addWidget(bt);
        buttons.append(bt);
    }

    bt =  new QMyButton(this);
    bt->setMinimumSize(QSize( buttonwidth, buttonHeight));
    bt->setCheckable(true);
    bt->setChecked(false);
    bt->setObjectName(QString::number(i));
    layRow4->addWidget(bt);
    buttons.append(bt);
    i++;

    bt =  new QMyButton(this); //Enter
    bt->setMinimumSize(QSize(60, buttonHeight));
    bt->setDefault(true);
    bt->setObjectName(QString::number(i));
    layRow4->addWidget(bt);
    buttons.append(bt);
    i++;
    
    layRow4->addSpacing(2);

    bt =  new QMyButton(this); //PgDn
    bt->setMinimumSize(QSize( specialButtonWidth, buttonHeight));
    bt->setObjectName(QString::number(i));
    layRow4->addWidget(bt);
    buttons.append(bt);
    i++;

    layKeyb->addLayout(layRow4);
    
    layRow5 = new QHBoxLayout();
    layRow5->setSpacing(1);

    bt =  new QMyButton(this); //shift
    bt->setMinimumSize(QSize(bigButtonWidth, buttonHeight));
    bt->setCheckable(true);
    bt->setChecked(false);
    bt->setObjectName(QString::number(i));
    layRow5->addWidget(bt);
    modKeys.append(bt);
    buttons.append(bt);
    i++;

    
    for(; i < 70; i++) {
        bt =  new QMyButton(this);
        bt->setMinimumSize(QSize( buttonwidth, buttonHeight));
        bt->setObjectName(QString::number(i));
        layRow5->addWidget(bt);
        buttons.append(bt);

    }
    
    bt = new QMyButton(this); //shift
    bt->setMinimumSize(QSize(bigButtonWidth, buttonHeight));
    bt->setObjectName(QString::number(i));
    bt->setCheckable(true);
    bt->setChecked(false);
    layRow5->addWidget(bt);
    buttons.append(bt);
    modKeys.append(bt);
    i++;

    layRow5->addSpacing(30);

    bt =  new QMyButton(this); //End
    bt->setMinimumSize(QSize( specialButtonWidth, buttonHeight));
    bt->setObjectName(QString::number(i));
    layRow5->addWidget(bt);
    buttons.append(bt);
    i++;

    layKeyb->addLayout(layRow5);

    layRow6 = new QHBoxLayout();
    layRow6->setSpacing(1);

    for(; i < 75; i++) { //left ctrl win alt
        bt =  new QMyButton(this);
        bt->setMinimumSize(QSize( specialButtonWidth, buttonHeight));
        layRow6->addWidget(bt);
        bt->setCheckable(true);
        bt->setObjectName(QString::number(i));
        buttons.append(bt);
        modKeys.append(bt);
    }

    bt =  new QMyButton(this); //space
    bt->setMinimumSize(QSize(150, buttonHeight));
    bt->setObjectName(QString::number(i));
    layRow6->addWidget(bt);
    buttons.append(bt);
    i++;

    for(; i < 77; i++) { //alt ctl
        bt =  new QMyButton(this);
        bt->setMinimumSize(QSize( specialButtonWidth, buttonHeight));
        layRow6->addWidget(bt);
        bt->setCheckable(true);
        bt->setObjectName(QString::number(i));
        buttons.append(bt);
        modKeys.append(bt);
    }

    bt =  new QMyButton(this); //delete
    bt->setMinimumSize(QSize( specialButtonWidth, buttonHeight));
    layRow6->addWidget(bt);
    bt->setObjectName(QString::number(i));
    buttons.append(bt);

    for(; i < 81; i++) { //arrow
        bt =  new QMyButton(this);
        bt->setMinimumSize(QSize( buttonwidth, buttonHeight));
        layRow6->addWidget(bt);
        bt->setObjectName(QString::number(i));
        buttons.append(bt);
    }
    
    layKeyb->addLayout(layRow6);
    
    mainFormLayout->addLayout(layKeyb);
    mainFormLayout->setSpacing(1);

    kbName = shareDir + "/english.xml";
    readSettings();

    setButtonText("text");
    
    for ( int a = 0; a < buttons.size(); a++ ){
        QMyButton *btn = buttons[a];
        connect(btn, SIGNAL(keyClick(unsigned int )),
                this, SLOT(keyPress(unsigned int)));
    }

    QDesktopWidget *desktopWidget = QApplication::desktop();
    QRect screenRect = desktopWidget->availableGeometry(this);

    int DesktopHeight = screenRect.height();
    int DesktopWidth = screenRect.width();
    int widgetHeight = this->height();
    int widgetWidth = this->width();

    move((DesktopWidth / 2) - widgetWidth,
          DesktopHeight - (widgetHeight));
    drag = false;
    rsz = false;
  //  resize(424,137);

    createActions();
    createTrayIcon();
    connect(trayIcon, &QSystemTrayIcon::messageClicked, this, &mainForm::messageClicked);
    connect(trayIcon, &QSystemTrayIcon::activated, this, &mainForm::iconActivated);

  //  QTimer::singleShot(0,this,SLOT(hideApp()));
}

mainForm::~mainForm()
{
}

void mainForm::setVisible(bool visible)
{
    QDialog::setVisible(visible);
}

void mainForm::readSettings()
{
    QSettings settings("Trolltech","QVKeyboard");

    QFont font;
    currentFont = settings.value("qvkeyboard/font").toString();
    if( currentFont.isEmpty()) {
        changeFont();
    } else {
        font.fromString(currentFont);
        currentFont = font.toString();
        setFont( font);
    }

    if (!settings.value("qvkeyboard/lang").isNull()) {
        //  qWarning("keyboard is empty");
        kbName = "://english.xml";
//        shareDir + "/"
//                + settings.value("qvkeyboard/lang", "/english").toString()
//                + ".xml";

        //      for (uint i = 0; i < lang->count();iiiiii  ++i)
        //       if (lang->text(i) == settings.value("qvkeyboard/lang"))
        //         lang->setItemChecked(lang->idAt(i), TRUE);
        //     else
        //       lang->setItemChecked(lang->idAt(i), FALSE);
    }

    // settings.endGroup();
}

void mainForm::writeSettings()
{
    QSettings settings("Trolltech","QVKeyboard");

    settings.beginGroup("qvkeyboard");
    settings.setValue("font", currentFont);
    
    //     for (uint i = 0; i < lang->count(); ++i)
    //         if (lang->isItemChecked(lang->idAt(i)))
    //             settings.setValue("lang", lang->text(i));
    //         else
    settings.setValue("lang", "english");
    settings.endGroup();
}

void mainForm::setButtonText(QString type)
{
    //    qWarning()<<"setButtonText"<<type;
    QDomDocument doc("keyboard");
    QDomElement root;
    QDomElement key;

    QFile file(kbName);
    if (!file.open( QIODevice::ReadOnly)) {
        qDebug() << kbName << " file doesn't exist";
        return;
    } else {
        QTextStream stream(&file);
        if (!doc.setContent( stream.readAll())) {
            qDebug("can not set content ");
            file.close();
            return;
        } else {
            root = doc.documentElement();
            key = root.firstChild().toElement();
        }
    }

    int i = 0;

    for (QDomNode n = root.firstChild(); !n.isNull(); n = n.nextSibling()) {
        QMyButton *btn = buttons[i];
        //     qWarning()<<n.toElement().attribute(type);
        btn->setText( n.toElement().attribute(type));
        btn->setKeyCode( n.toElement().attribute("keycode").toInt());
        btn->setMouseTracking(true);
        i++;
    }
    file.close();
}




void mainForm::changeFont()
{
    qWarning()<< "changeFOnt";
    QFont font;
    QSettings settings("Trolltech","QVKeyboard");

    bool ok;

    font.fromString(currentFont);
    font  = QFontDialog::getFont(&ok, font, this);
    if (ok) {
        currentFont = font.toString();
        font.fromString(currentFont);
        currentFont = font.toString();
        writeSettings();
        setFont( font);
        //      this->resize( this->sizeHint());
    }
}

void mainForm::changeLanguage(int )
{
    //     for (uint i = 0; i < lang->count(); ++i)
    //         lang->setItemChecked(lang->idAt(i), FALSE);

    //     lang->setItemChecked(lang->idAt(x), TRUE);
    //     kbName = shareDir + "/" + lang->text(x) + ".xml";
    //     setButtonText("text");
    //     writeSettings();
}

void mainForm::about()
{
    QMessageBox::information(this, "About QVKeyboard",
                             "<P>QVKeyboard was written by lpotter@trolltech.com and "
                             "contains Software written by moux http://www.moux.pl "
                             "and Todor Gyumyushev yodor@developer.bg </p><p>Licensed under the GPL v2");
}

void mainForm::keyPress( unsigned int a )
{
    if(a == 0) return;
    
    QMyButton *btn;
    btn = qobject_cast<QMyButton *>(this->sender());
    //    qWarning() << "keyPress objectName" << btn->objectName();
    
    if( buttons[58]->isChecked() //shift
            || buttons[69]->isChecked() )
        setButtonText("text");


    if(btn->text() == "Caps") {
        if ( !btn->isChecked()) {
            setButtonText("caps");
        } else {
            setButtonText("text");
        }
    }
    
    if ( btn->text() == "Shift" )
        if ( !btn->isChecked()) {
            setButtonText("shift");
        }

    sendKey( a,true,true );

    for ( int a = 0; a < modKeys.size(); a++ ) {
        if( modKeys[a]->isChecked())  modKeys[a]->setChecked( false );
    }
}

void mainForm::sendKey( unsigned int keycode, bool press, bool release )
{
    Q_UNUSED(press);
    Q_UNUSED(release);
    Window curr_focus;
    int revert_to;

    XGetInputFocus( display, &curr_focus, &revert_to );

    for ( int a = 0; a < modKeys.size(); a++ ) {
        if ( modKeys[a]->isChecked() ) {
            XTestFakeKeyEvent( display, modKeys[a]->getKeyCode(), true, 0 );
        }
    }

    XTestFakeKeyEvent( display, keycode, true,1 );
    XTestFakeKeyEvent( display, keycode, false, 2 );

    for ( int a = 0; a < modKeys.size(); a++ ) {
        if ( modKeys[a]->isChecked() ) {
            XTestFakeKeyEvent( display, modKeys[a]->getKeyCode(), false, 2 );
        }
    }

    XFlush( display );
}

void mainForm::keyPressEvent( QKeyEvent * event )
{
    event->ignore();
}

void mainForm::keyReleaseEvent( QKeyEvent * event )
{
    event->ignore();
}

void mainForm::mousePressEvent(QMouseEvent *e)
{
    QPoint pos = e->pos();
    if (pos.x() > width() - 20 && pos.x() < width()
            && pos.y() > height()-20 && pos.y() < height() ) {
        rsz = true;
        dragP = QPoint( width() - e->pos().x(), height() - e->pos().y());
    }
    else {
        dragP = e->pos();
        drag = true;
    }
}

void mainForm::mouseReleaseEvent(QMouseEvent *)
{
    if (!rsz){
        drag = false;
        return;
    }
    rsz = false;
}

void mainForm::mouseMoveEvent(QMouseEvent *e)
{
    if (!rsz) {
        if (!drag) {
            return;
        }
        QPoint curr( e->globalPos().x() - dragP.x()
                     , e->globalPos().y() - dragP.y());
        QWidget::move(curr);
        return;
    }
    QPoint curr( e->globalPos().x(), e->globalPos().y());
    QPoint pos = QWidget::pos();
    //QPoint curr(e->globalPos().x()-dragP.x(),e->globalPos().y()-dragP.y());
    int nw = curr.x() - pos.x() + dragP.x();
    int nh = curr.y() - pos.y() + dragP.y();

    resize(nw,nh);
}

void mainForm::paintEvent(QPaintEvent *)
{
    QPainter p(this);

    for (int a=0;a<20;a+=5){

        p.setPen( QColor( 170,170,0));
        p.drawLine( width() - 15 + a, height() - 2, width() - 2
                    , height() - 15 + a);
        p.setPen( QColor( 200,200,0));
        p.drawLine( width() - 14 + a, height() - 2, width() - 2
                    , height() - 14 + a);
    }
}

void mainForm::createTrayIcon()
{
    trayIconMenu = new QMenu(this);
    trayIconMenu->addAction(minimizeAction);
    trayIconMenu->addAction(maximizeAction);
    trayIconMenu->addAction(restoreAction);
    trayIconMenu->addSeparator();
    trayIconMenu->addAction(quitAction);

    trayIcon = new QSystemTrayIcon(this);
    trayIcon->setContextMenu(trayIconMenu);

    QIcon icon = QIcon(":/qvktray.png");
    trayIcon->setIcon(icon);
    trayIcon->show();
}

void mainForm::createActions()
{
    minimizeAction = new QAction(tr("Mi&nimize"), this);
    connect(minimizeAction, &QAction::triggered, this, &QWidget::hide);

    maximizeAction = new QAction(tr("Ma&ximize"), this);
    connect(maximizeAction, &QAction::triggered, this, &QWidget::showMaximized);

    restoreAction = new QAction(tr("&Restore"), this);
    connect(restoreAction, &QAction::triggered, this, &QWidget::showNormal);

    quitAction = new QAction(tr("&Quit"), this);
    connect(quitAction, &QAction::triggered, qApp, &QCoreApplication::quit);
}

void mainForm::messageClicked()
{
    QMessageBox::information(0, tr("Systray"),
                             tr("Sorry, I already gave what help I could.\n"
                                "Maybe you should try asking a human?"));
}


void mainForm::iconActivated(QSystemTrayIcon::ActivationReason reason)
{
    switch (reason) {
    case QSystemTrayIcon::Trigger:
    case QSystemTrayIcon::DoubleClick:
        showKeyboard();
        break;
    case QSystemTrayIcon::MiddleClick:
        break;
    default:
        ;
    }
}

void mainForm::hideApp()
{
    if (trayIcon->isVisible()) {
        hide();
    }
}

void mainForm::setIcon(int /*index*/)
{
    QIcon icon = QIcon(":/qvktray.png");
    trayIcon->setIcon(icon);
    trayIcon->show();
}

void mainForm::closeEvent(QCloseEvent *event)
{
    if (trayIcon->isVisible()) {
        hide();
        event->ignore();
    }
}

void mainForm::showKeyboard()
{
    if (this->isVisible()) {
        hide();
    } else {
        show();
    }

// //   if (!trayIcon->isVisible()) {
//        //    QWidget *container = QWidget::createWindowContainer(view, this);
//        //    container->setFocusPolicy(Qt::TabFocus);

//        QQuickView *view = new QQuickView();

//        //    QQuickView view(QString("qrc:/qvktray.qml"));
//        view->setWidth(1200);
//        view->setHeight(800);

//        view->setSource(QUrl("qrc:/qvktray.qml"));
//        //  this->verticalLayout->addWidget(container);

//        qDebug() << Q_FUNC_INFO << view->status();
//        if (view->status() == QQuickView::Error) {
//            qDebug() << Q_FUNC_INFO << "ERROR";
//            return ;
//        }
//        view->setResizeMode(QQuickView::SizeRootObjectToView);

//        view->show();
  //  }
}
