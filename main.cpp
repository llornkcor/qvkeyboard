#include <qapplication.h>
#include <QMessageBox>

#include "mainfrm.h"

#include <X11/Xlib.h>

void findAloneWindow()
{
    unsigned int numkids, i, scrn;
    Window r, p;
    Window *kids=0;
    //XWindowAttributes attr;
    Window root;
    Display *dipsy=0;
    char *win_name=0;

    dipsy = XOpenDisplay(0);
    if (!dipsy)return;

    scrn = DefaultScreen(dipsy);
    root = RootWindow(dipsy, scrn);

    XQueryTree(dipsy, root, &r, &p, &kids, &numkids);


    for (i = 0; i < numkids;  ++i)
    {
        XFetchName(dipsy, kids[i], &win_name);
        QString c(win_name);

        //  if (c=="kvkbdalone")
        //   {
        //      long wid = kids[i];
        // XDestroyWindow(dipsy,wid);
        // XFlush(dipsy);
        // i=numkids;
        // }
        XFree(win_name);
    }
    XCloseDisplay(dipsy);
}

int main(int argc, char **argv)
{
    QApplication a(argc,argv);
    if (!QSystemTrayIcon::isSystemTrayAvailable()) {
        QMessageBox::critical(0, QObject::tr("QVkTray"),
                              QObject::tr("No system tray on this system."));
        return 1;
    }

    mainForm w(NULL);
    findAloneWindow();
    w.show();
    a.connect(&a, SIGNAL(lastWindowClosed()), &a, SLOT(quit()));
    return a.exec();
}

