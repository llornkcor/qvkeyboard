/**
 *  nie wiem co tu dodac
 **/

#ifndef MAINFORM_H
#define MAINFORM_H

#include "qmybutton.h"

#include <QDialog>
#include <QMenu>
#include <QLayout>

#include <QTextStream>
#include <qfontdialog.h>
#include <QMessageBox>
#include <QSettings>
#include <qfont.h>
#include <qfile.h>
#include <qaction.h>
#include <qtextcodec.h>
#include <qdir.h>
#include <qapplication.h>
#include <QVector>
#include <QKeyEvent>

#include <QSystemTrayIcon>
#include <QtX11Extras/qx11info_x11.h>

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QSpacerItem;
class QAction;
class QActionGroup;
class QToolBar;
class QMenu;
class QTextEdit;
class QMyButton;

class mainForm : public QDialog {
    Q_OBJECT
public:
    mainForm(QWidget * parent = 0, Qt::WindowFlags fl = Qt::Tool
            | Qt::WindowStaysOnTopHint
            | Qt::X11BypassWindowManagerHint
            );
    ~mainForm();

    QTextEdit *editor;
    QString kbName;
    QMenu *options;
    QMenu *lang;
    QString shareDir;
public slots:

    void keyPress(unsigned int keycode);

    void changeFont();
    void changeLanguage(int);
    void about();
    void readSettings();
    void writeSettings();

    void setVisible(bool visible) override;

protected:

    QVBoxLayout * mainFormLayout;
    QVBoxLayout *layKeyb;
    QHBoxLayout *layFirstRow1;
    QHBoxLayout *layFirstRow;
    QHBoxLayout *layRow2;
    QHBoxLayout *layRow3;
    QHBoxLayout *layRow4;
    QHBoxLayout *layRow5;
    QHBoxLayout *layRow6;

    void mouseMoveEvent ( QMouseEvent * e );
    void mousePressEvent ( QMouseEvent * e );
    void mouseReleaseEvent ( QMouseEvent * e );
    virtual void paintEvent ( QPaintEvent *e );
    void closeEvent(QCloseEvent *event) override;

    QPoint dragP;
protected slots:
    virtual void setButtonText(QString type);

    void setIcon(int index);
    void iconActivated(QSystemTrayIcon::ActivationReason reason);
    void messageClicked();
    void hideApp();
    void showKeyboard();

private:

    bool drag;
    bool rsz;
    QString currentFont;
    QVector<QMyButton *> buttons;
    QVector<QMyButton *> modKeys;
    
    Display *display;
    void sendKey(unsigned int keycode, bool press, bool release);
    void keyPressEvent( QKeyEvent * event );
    void keyReleaseEvent( QKeyEvent * event );

    void createActions();
    void createTrayIcon();

    QAction *minimizeAction;
    QAction *maximizeAction;
    QAction *restoreAction;
    QAction *quitAction;

    QSystemTrayIcon *trayIcon;
    QMenu *trayIconMenu;

};

#endif // MAINFORM_H

